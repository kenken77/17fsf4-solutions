module.exports = {
  domain_name: "http://localhost:3000",
  PORT: 3000,

  AWS_S3_REGION: 'ap-southeast-1',
  AWS_S3_BUCKET: 'cjinpheow-17fsf2',

  version: '1.0.0'
};