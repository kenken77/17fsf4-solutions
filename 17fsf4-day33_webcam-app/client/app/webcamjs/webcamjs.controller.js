(function() {
  angular
    .module('WCA')
    .controller('WebcamJSController', WebcamJSController);

  WebcamJSController.$inject = [ '$http' ];

  function WebcamJSController($http) {
    var vm = this;

    var _video = null,
      patData = null,
      activeStream = null;

    vm.patOpts = {};
    vm.isTurnOn = false;
    vm.webcamError = false;

    vm.channel = {}; 
    
    vm.onError = function(err) {
      // console.log("onError");
      console.log(err);
    }

    vm.onStream = function(stream) {
      // console.log("onStream");
      activeStream = stream;
    }

    vm.onSuccess = function() {
      // console.log("onSuccess");
      _video = vm.channel.video;
      vm.patOpts.w = _video.width;
      vm.patOpts.h = _video.height;
      vm.isTurnOn = true;
    }

    var getVideoData = function getVideoData(x, y, w, h) {
      var hiddenCanvas = document.createElement('canvas');
      hiddenCanvas.width = _video.width;
      hiddenCanvas.height = _video.height;
      var ctx = hiddenCanvas.getContext('2d');
      ctx.drawImage(_video, 0, 0, _video.width, _video.height);
      return ctx.getImageData(x, y, w, h);
    };

    var sendSnapshotToServer = function sendSnapshotToServer(imgBase64) {
      vm.snapshotData = imgBase64;

      // get the extension
      var ext = vm.snapshotData.split(';')[0].match(/jpeg|png|gif/)[0];
      // strip off the data: url prefix to get just the base64-encoded bytes
      var data = vm.snapshotData.replace(/^data:image\/\w+;base64,/, "");

      // set up data to upload to server
      var imageToUpload = {
        filename: 'image.' + ext,
        imagedata: data,
      }

      $http.post('/snapshot', imageToUpload)
        .then(function(result) {
          console.log(result.data);
        })
        .catch(function(err) {
          console.log(err);
        });

      // At the server, convert to a file using the following code:      
      // var buf = new Buffer(imagedata, 'base64');
      // fs.writeFile(filename, buf);
    };

    vm.makeSnapshot = function() {
      if (_video) {
        var patCanvas = document.querySelector('#snapshot');
        if (!patCanvas) return;

        patCanvas.width = _video.width;
        patCanvas.height = _video.height;
        var ctxPat = patCanvas.getContext('2d');

        var idata = getVideoData(vm.patOpts.x, vm.patOpts.y, vm.patOpts.w, vm.patOpts.h);
        ctxPat.putImageData(idata, 0, 0);

        sendSnapshotToServer(patCanvas.toDataURL());

        vm.patData = idata;
      }
    }

    vm.turnOff = function () {
      console.log("turnOff");
      vm.isTurnOn = false;
      if (activeStream && activeStream.getVideoTracks) {
        const checker = typeof activeStream.getVideoTracks === 'function';
        if (checker) {
          return activeStream.getVideoTracks()[0].stop();
        }
        return false;
      }
      return false;
    };
  }
})();
