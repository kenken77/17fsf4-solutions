(function() {
  angular
    .module("FUL")
    .controller("FulCtrl", FulCtrl);

  FulCtrl.$inject = ["Upload"];

  function FulCtrl(Upload) {
    var vm = this;
    
    vm.file = null;
    vm.fileS3 = null;

    vm.upload = upload;
    vm.uploadS3 = uploadS3;

    function upload() {
      console.log(vm.file);
      Upload.upload({
        url: '/upload',
        data: {
          img_file: vm.file,
        }
      })
      .then(function(res) {
        console.log(res);
      })
      .catch(function(err) {
        console.log(err);
      });
    }

    function uploadS3() {
      console.log(vm.fileS3);
      Upload.upload({
        url: '/uploadS3',
        data: {
          img_file: vm.fileS3,
        }
      })
      .then(function(res) {
        console.log(res);
      })
      .catch(function(err) {
        console.log(err);
      });
    }

  }



})();