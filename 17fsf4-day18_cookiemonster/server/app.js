// load express
var express = require('express');

// cookie parser middleware to parse cookies
var cookieParser = require('cookie-parser');

// load configuration
var config = require('./config');

// create express instance
var app = express();

// set server port
const PORT = process.env.PORT || process.env.NODE_PORT || config.PORT || 3000;

// apply cookie parser middleware which will parse client cookies
// as req.cookies object
app.use(cookieParser());

// middleware to print out request headers
app.use(function(req, res, next) {
  console.log("***** START OF REQUEST HEADER *****");
  console.log(req.headers);
  console.log("***** END OF REQUEST HEADER *******\n");

  next();
});

// view set cookies
app.get("/cookie", function(req, res) {
  // req.cookies populated by cookieParser middleware
  console.log("Cookies sent in request: ")
  console.log(req.cookies);
  res.status(200).json(req.cookies);
});

// set one or more cookies
app.get('/setcookie', function(req, res) {
  for (var key in req.query) {
    if (req.query[key]) {
      // setting cookie
      console.log("Setting cookie %s => %s", key, req.query[key]);
      res.cookie(key, req.query[key], 
        { // options
          maxAge: 60000, // 60000 milliseconds => 60 seconds
        }
      );
    }
  }
  res.status(200).send('Cookie set');
});

// clear one cookie
app.get('/clearcookie', function(req, res) {
  for (var key in req.query) {
    console.log("Clearing cookie %s", key);
    res.cookie(key, '', { maxAge: 0}); // or just res.clearCookie(key);
  }
  res.status(200).send("Cookie cleared");
});

// clearing all cookies
app.get('/clearallcookies', function(req, res) {
  for (var key in req.cookies) {
    console.log("Clearing cookie %s", key);
    res.clearCookie(key);
  }
  res.status(200).send("No more cookies!");
});

// start server
app.listen(PORT, function() {
  console.log("Server started on port %d...", PORT);
});
