var express = require('express');
var MongoClient = require('mongodb').MongoClient;

var app = express();
// Connection URL
var url = 'mongodb://localhost:27017/michelin';
// Use connect method to connect to the Server

const port = 3000;
db = null;
var ObjectID = require('mongodb-core').BSON.ObjectID

MongoClient.connect(url)
  .then(function(db) {
    this.db = db;

    app.listen(port, function() {
      console.log("Server started on port %d...", port);
    });
  })
  .catch(function(err) {
    console.log(err);
  });

app.get("/restaurants", function(req, res) {
  console.log(req.query);
  var restaurants = db.collection('restaurants');
  restaurants.find(
    req.query
    //, {_id: false}
  )
    .limit(5)
    .sort({ name: 1})
    .toArray()
    .then(function(results) {
      res.json(results);
    });
});

app.get("/restaurants/:_id", function(req, res) {
  console.log(req.params);
  var restaurants = db.collection('restaurants');
  restaurants.findOne(
    {_id: ObjectID(req.params._id)}
  )
    .then(function(result) {
      res.json(result);
    });
});
