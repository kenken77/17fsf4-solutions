var express = require('express');
var app = express();
var path = require('path');

var bodyParser = require('body-parser');
var session = require('express-session');

var passport = require('passport');

var config = require('./config');
var PORT = process.env.PORT || process.env.NODE_PORT || config.PORT || 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// app.use(function(req, res, next) {
//   console.log('\nOriginalUrl --->>> ' + req.originalUrl);
//   console.log('req.body --->>> ' + JSON.stringify(req.body));
//   next();
// });

app.use(session({
  secret: config.SECRET,
  resave: true,
  saveUninitialized: true,
}));

app.use(passport.initialize());
app.use(passport.session());

app.use(express.static(path.join(__dirname, '/../client')));

var auth = require('./auth')(app, passport);
require('./routes')(auth, app, passport);

app.listen(PORT, function() {
  console.log("Server started on port %d...", PORT);
});
