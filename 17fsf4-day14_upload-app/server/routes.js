var config = require('./config');

var fs = require('fs'),
  path = require('path'),
  multer = require('multer'),
  AWS = require('aws-sdk'),
  multerS3 = require('multer-s3'),
  storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.join(__dirname, "/uploads/"));
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + '-' + file.originalname);
    }
  }),
  upload = multer({
    storage: storage,
  });

AWS.config.region = config.AWS_S3_REGION;
console.log("AWS Bucket init...");
var s3Bucket = new AWS.S3();

var uploadS3 = multer({
    storage: multerS3({
        s3: s3Bucket,
        bucket: config.AWS_S3_BUCKET,
        metadata: function(req, file, cb) {
            cb(null, {fieldName: file.fieldname});
        },
        key: function(res, file, cb) {
            cb(null, Date.now() + '-' + file.originalname);
        }
    })
});

module.exports = function(app) {
  app.post("/uploadS3", 
    uploadS3.single("img_file"),
    function(req, res) {
      console.log("Upload to S3...");
      console.log(req.file);
      res.status(200).send("Successfully uploaded to S3");
    }
  );

  app.post("/upload",
    upload.single("img_file"),
    function(req, res) {
      console.log("Upload to server...");
      console.log(req.file);
      console.log("Name: %s", req.file.originalname);
      
      res.status(200).end();
    }
  );
}
