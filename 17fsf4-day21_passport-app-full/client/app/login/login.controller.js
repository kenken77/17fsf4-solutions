(function() {
  angular
    .module('PAF')
    .controller('LoginCtrl', LoginCtrl);

  LoginCtrl.$inject = [ 'PassportSvc', '$state' ];

  function LoginCtrl(PassportSvc, $state) {
    var vm = this;

    vm.user = {
      username: '',
      password: '',
    }
    vm.msg = '';

    vm.login = login;

    function login() {
      PassportSvc.login(vm.user)
        .then(function(result) {
          $state.go('home');
          return true;
        })
        .catch(function(err) {
          vm.msg = 'Invalid Username or Password!';
          vm.user.username = vm.user.password = '';
          return false;
        });
    }
  }
})();
