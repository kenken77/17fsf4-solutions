(function() {
    angular.module("ShoppingCartApp")
      .controller("CartCtrl", CartCtrl)

    CartCtrl.$inject = ['CartService', 'growl']

    function CartCtrl(CartService, growl) {
        var vm = this

        vm.newItem = {}
        vm.cart = []
        vm.msg = ""
        vm.totalCartCnt = 0;

        CartService.refreshCart(vm.newItem.userId)
        .then(function(items) {
            console.log(items);
            vm.cart = items;
            console.log(items.length);
            for(var y=0 ; y < items.length; y++){
                vm.totalCartCnt = vm.totalCartCnt + parseInt(items[y].quantity);
            }
        })
        .catch(function() {
            vm.msg = "Error fetching your cart!"
        })

        vm.addToCart = function() {

            CartService.addToCart(vm.newItem)
              .then(function() {
                  vm.msg = "Item was added to cart successfully";
                  vm.totalCartCnt = vm.totalCartCnt + parseInt(vm.newItem.quantity);
                  vm.cart.push({name: vm.newItem.name, quantity: vm.newItem.quantity });
                  growl.info(`${vm.newItem.name} was added to your shopping cart -> ${vm.newItem.quantity}`); 
              })
              .catch(function() {
                  vm.msg = "There was some problem adding item to cart"
              })
        }

        vm.refreshCart = function() {
            CartService.refreshCart(vm.newItem.userId)
              .then(function(items) {
                  console.log(items);
                  vm.cart = items;
                  vm.newItem = {};
              })
              .catch(function() {
                  vm.msg = "Error fetching your cart!"
              })
        }

        vm.checkout = function() {
            CartService.checkout()
              .then(function() {
                  vm.msg = vm.cart.length  + " items Checked out!";
                  growl.success(`Successful checkout ${vm.totalCartCnt} items `); 
                  vm.cart = [];
                  vm.newItem = {}
                  vm.totalCartCnt = 0;
                  
              })
              .catch(function() {
                  vm.msg = "Error fetching your cart!"
              })
        }
    }
})()