var express = require('express');
var session = require('express-session');
var config  = require('./config');

const PORT = process.env.NODE_PORT || config.PORT || 3000;

var app = express();

app.use(session({
  secret: config.SESSION_SECRET,
  resave: false,
  saveUninitialized: true,
  cookie: { secure: false },
}));

app.use(function(req, res, next) {
  console.log("***** START OF REQUEST HEADER *****");
  console.log(req.headers);
  console.log("***** END OF REQUEST HEADER *******\n");  
  console.log("** Session ***");
  console.log('Session ID: %s', req.session.id);
  console.log(req.session);

  next();
});

app.get("/home", function(req , res) {
  req.session.mycookie1 = 'Almond Nuts';
  res.status(200).send("My Home");
});

app.get("/one", function(req , res) {
  res.status(200).send("One");
});

app.listen(PORT, function() {
  console.log("Server stared on port %d...", PORT);
});
