# A Complete Passport-Local Strategy Implementation

## Using a JSON object (instead of DB) to store the username/password information:
```
[
  {
    username: 'adam',
    password: 'mada',
  },
  {
    username: 'betty',
    password: 'ytteb',
  },
  {
    username: 'charles',
    password: 'selrahc',
  },
]
```

## Use of a `resolve block` feature in ui-routing to check if user is signed in.

`File: client/app-route-config.js:`
```
  resolve: {
    user: function(PassportSvc) {
      return PassportSvc.userAuth()
        .then(function(result) {
          return result.data.user;
        })
        .catch(function(err) {
          return '';
        });
    }
  },
```

`File: app/home/home.controller.js:`
```
(function() {
  angular
    .module('PAF')
    .controller('HomeCtrl', HomeCtrl);

  HomeCtrl.$inject = [ 'user' ];

  function HomeCtrl(user) {
    var vm = this;

    vm.user = user;
  }
})();
```

## Use of `named views` in ui-routing to have multiple ui-views tags.

`File: client/app-route-config.js:`
```
  .state("login", {
    url: "/login",        
    views: {
      'menu': {
        templateUrl: 'app/menu/menu.html',
        controller: 'MenuCtrl as ctrl',
      },
      'content': {
        templateUrl: 'app/login/login.html',
        controller: 'LoginCtrl as ctrl',
      }
    },
```

Author: cjinpheow@gmail.com
