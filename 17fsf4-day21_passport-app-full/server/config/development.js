'use strict';

module.exports = {
  PORT: 3000,

  SECRET: 'stackup',

  USER_DATABASE: [
    {
      username: 'adam',
      password: 'mada',
    },
    {
      username: 'betty',
      password: 'ytteb',
    },
    {
      username: 'charles',
      password: 'selrahc',
    },
    {
      username: 'denise',
      password: 'esined',
    },
    {
      username: 'eric',
      password: 'cire',
    }
  ],

  version: '1.0',
};
