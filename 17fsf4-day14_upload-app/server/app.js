// Exports AWS keys as environment variables (see server/awsAccessKeys)

var config = require('./config');

var express = require('express');
var path = require('path');

var app = express();

var PORT = process.env.NODE_PORT || process.argv[2] || config.PORT || 3000;

const CLIENT_FOLDER = path.join(__dirname, '/../client');
const MESSAGES_FOLDER = path.join(CLIENT_FOLDER, '/assets/messages');

app.use(express.static(CLIENT_FOLDER));

require('./routes')(app);

app.use(function (req, res) {
    res.status(404).sendFile(MESSAGES_FOLDER + '/404.html');
});

app.use(function (err, req, res, next) {
    res.status(500).sendFile(MESSAGES_FOLDER + '/500.html');
});

app.listen(PORT, function() {
  console.log("Server started on port %d...", PORT);
});
