var express = require('express');
var path = require('path');
var fs = require('fs');

var bodyParser = require('body-parser');

var app = express();

const port = process.env.PORT || 3000;

app.use(express.static(path.join(__dirname, '../client')));

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

app.post('/snapshot', function(req, res) {
  var buf = new Buffer(req.body.imagedata, 'base64');
  var filename = Date.now() + '_' + req.body.filename;
  console.log(filename);
  console.log(req.body.imagedata.length);
  fs.writeFile(__dirname + '/../client/snapshots/' + filename, buf);
  res.status(200).json({savedAsFile: filename});
});

app.listen(port, function() {
  console.log("Server listening on port %d...", port);  
});
