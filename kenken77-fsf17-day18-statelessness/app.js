var express = require('express');

var bodyParser = require('body-parser');
var cookieSession = require('cookie-session');
var session = require('express-session');
const uuidv4 = require('uuid/v4');

const PORT = process.argv[2] || process.env.NODE_PORT || 3000;

var app = express();

app.use(
    cookieSession({
        secret: "azxsw23e%t1cdsf",
        resave: false,
        saveUninitialized: true,
        httpOnly: false
    })
);

app.use(function(req,res,next){
    req.session.id = req.session.id || uuidv4() 
    if(! req.session.cart ){
        req.session.cart = [];
    }
    next();
})

app.use(express.static(__dirname + "/public"));
app.use("/bower_components", express.static(__dirname + "/bower_components"));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));

app.post("/api/cart/add", function(req,res){
    req.session.cart.push({
        sessionid: req.session.id,
        name: req.body.name,
        quantity: req.body.quantity
    });
    console.log(JSON.stringify(req.session.cart));
    res.status(202).end();
});

app.get("/api/cart/refresh", function(req,res){
    var currentUserCart  = [];
    
    var everything = req.session.cart;
    for(var x =0; x < everything.length; x++ ){
        try{
            if(req.session.id === everything[x].sessionid){
                currentUserCart.push(everything[x]);
            }
        }catch(e){
            continue;
        }
    }
    res.status(200).json(currentUserCart);
})

app.post("/api/cart/checkout", function(req,res){
    
    var everything = req.session.cart;
    for(x = 0; x < everything.length; x ++ ){
        try{    
            if(everything[x].sessionid == req.session.id){
                delete req.session.cart[x];
                //call db and payement module.
            }
        }catch(e){
            continue;
        }
    }
    res.status(202).end();
})

app.listen(PORT,function(){
    console.log("Server is running on ", PORT);
})