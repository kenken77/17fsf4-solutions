(function(){
    angular
        .module("ShoppingCartApp", ['angular-notification-icons', 'ngAnimate', 'angular-growl']);
})();